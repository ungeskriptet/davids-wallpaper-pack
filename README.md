# David's Wallpaper Pack

To install this on Arch-based distros:
```
git clone https://gitlab.com/Ungeskriptet/davids-wallpaper-pack.git
cd davids-wallpaper-pack
makepkg -si
```

Some wallpapers are taken from [r/wallpaper](https://reddit.com/r/wallpaper), [r/wallpapers](https://reddit.com/r/wallpapers) or [wallhaven.cc](https://wallhaven.cc/).

